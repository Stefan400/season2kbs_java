/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;

import java.awt.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

import tztdatabase.Database;

public class PakketPageTracer extends JDialog {
    private Database db = new Database();
    private int id;
    private JTextArea traceField;

    public PakketPageTracer(int pakketID) {
        this.id = pakketID;
        this.setLayout(new GridLayout(1, 1));
        this.setModal(true);
        this.setTitle("Locatiegegevens: Pakket " + pakketID);
        this.setSize(480, 600);

        traceField = new JTextArea();
        traceField.setEditable(false);
        traceField.setText(getTraceData());
        this.add(traceField);
    }

    private String getTraceData() {
        String query = "SELECT `afhaalpuntStation` AS locatie, \n" +
                "`pakketVerzenddatum` AS opnametijd, \n" +
                "`pakketVerzenddatum` AS afgiftetijd \n" +
                "FROM `pakket` \n" +
                "LEFT JOIN `afhaalpunt` ON `pakketBeginpunt` = `afhaalpuntID` \n" +
                "WHERE `pakketID`= " + this.id + "\n" +
                "\n" +
                "UNION ALL\n" +
                "\n" +
                "SELECT `afhaalpuntStation` AS locatie,\n" +
                "`opname` AS opnametijd, \n" +
                "`afgifte` AS afgiftetijd \n" +
                "FROM pakket_afhaalpunt AS pa \n" +
                "LEFT JOIN `afhaalpunt` ON pa.afhaalpuntID = afhaalpunt.afhaalpuntID\n" +
                "WHERE `pakketID` = " + this.id + "\n" +
                "\n" +
                "UNION ALL\n" +
                "\n" +
                "SELECT sollicitant.sollicitantAchternaam AS locatie,\n" +
                "`opname` AS opnametijd, \n" +
                "`afgifte` AS afgiftetijd \n" +
                "FROM `pakket_werknemer` AS pw\n" +
                "LEFT JOIN `afhaalpunt` ON `opnameAfhaalpuntID` = afhaalpunt.afhaalpuntID\n" +
                "LEFT JOIN `werknemer` ON pw.werknemerID = werknemer.werknemerID\n" +
                "LEFT JOIN `sollicitant` ON werknemer.sollicitantID = sollicitant.sollicitantID\n" +
                "WHERE `pakketID` = " + this.id + "\n" +
                "\n" +
                "ORDER BY opnametijd ASC, afgiftetijd ASC;";

        ResultSet rs = db.selectSet(query);
        String returnString = "";

        try {
            while (rs.next()) {
                returnString = returnString + "Pakket is bij " + rs.getString("locatie") + "\n";
                returnString = returnString + "    - Opgenomen op " + rs.getString("opnametijd") + "\n";
                returnString = returnString + "    - Afgeleverd op " + rs.getString("afgiftetijd") + "\n \n";
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return returnString;
    }
}
