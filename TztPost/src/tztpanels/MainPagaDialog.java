package tztpanels;

import tztdatabase.Database;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.sql.*;
import java.util.ArrayList;


public class MainPagaDialog extends JFrame {//extends JDialog {

    private Database db = new Database();
    private ResultSet queryResult;
    private ResultSet countPerPakketEindpunt;
    private int stationNum, pakjesPerStation;
    // private String naamAfhaalpunt;
    private String tempStringX, tempStringY;
    private int inhoud;
    private int x = 0;
    private int index;
    private int tellert = 0;
    private ArrayList<String>[] resultArr = new ArrayList[15];

    private String naamAfhaalpunt, afhaalpunt, pakjesPerStationString;


    public MainPagaDialog(JFrame venster, int index) { //maakt een nieuw frame aan
        Font font = new Font("Verdana", Font.BOLD, 12);
        setFont(font);
        this.index = index;
        setSize(770, 455); //set frame
        setBackground(Color.blue); // set frame color
        setLayout(new BorderLayout());
        setTitle("JFrame dialogs");


        JLabel tekenContainer = new JLabel(); // nieuw JLabel
        tekenContainer.setOpaque(true);
        tekenContainer.setBackground(new Color(135, 134, 122)); //set background color voor indeling
        tekenContainer.setLayout(new BorderLayout()); //indeling pagina


        JLabel topLabel = new JLabel("Rendabiliteit");
        topLabel.setOpaque(true);
        topLabel.setBackground(new Color(76, 79, 72));


        //row1 Grid
        if (index == 0) {
            bepaalArray(resultArr); // System.out.println(this.resultArr[0]);

            MainPageTekenen tekenen = new MainPageTekenen(resultArr, tellert, index); // maakt het venster aan
            tekenContainer.add(topLabel, BorderLayout.NORTH, JLabel.CENTER); // label
            tekenContainer.add(tekenen, BorderLayout.CENTER); // voegt het venster toe aan het JLabel
            add(tekenContainer); // voegt aan het hoofdframe de JLabel toe met de gridlayout
        }
        if (index == 1) {
            //row grid 2
            bepaalArray(resultArr);

            MainPageTekenen tekenen2 = new MainPageTekenen(resultArr, tellert, index); // maakt het venster aan
            tekenContainer.add(topLabel, BorderLayout.NORTH, JLabel.CENTER); // label
            tekenContainer.add(tekenen2, BorderLayout.CENTER); // voegt het venster toe aan het JLabel
            add(tekenContainer); // voegt aan het hoofdframe de JLabel toe met de gridlayout
        }


    }

    public void bepaalArray(ArrayList<String>[] resultArr) {
        if (index == 0) {
            x=0;
            try {
                countPerPakketEindpunt = this.db.selectSet("SELECT af.afhaalpuntStation, af.afhaalpuntID, COUNT( * ) AS pakjesPerStation\n" +
                        "FROM afhaalpunt AS af\n" +
                        "JOIN pakket AS p ON af.afhaalpuntID = p.pakketEindpunt GROUP BY af.afhaalpuntStation ");

                while (countPerPakketEindpunt.next()) {
                    tellert++;
                    naamAfhaalpunt = countPerPakketEindpunt.getString("af.afhaalpuntStation");
                    stationNum = countPerPakketEindpunt.getInt("af.afhaalpuntID");
                    pakjesPerStation = countPerPakketEindpunt.getInt("pakjesPerStation");

                    tempStringX = Integer.toString(stationNum);
                    tempStringY = Integer.toString(pakjesPerStation);

                    for (x = x; x < tellert; x++) {

                        this.resultArr[x] = new ArrayList<>();
                        this.resultArr[x].add(naamAfhaalpunt);//,tempStringX,tempStringY);
                        this.resultArr[x].add(tempStringX);
                        this.resultArr[x].add(tempStringY);
                    }
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (index == 1) {
            try {
                countPerPakketEindpunt = this.db.selectSet("SELECT af.afhaalpuntStation, af.afhaalpuntID, COUNT( * ) AS pakjesPerStation\n" +
                        "FROM afhaalpunt AS af\n" +
                        "JOIN pakket AS p ON af.afhaalpuntID = p.pakketEindpunt GROUP BY af.afhaalpuntStation ");

                while (countPerPakketEindpunt.next()) {
                    
                    naamAfhaalpunt = countPerPakketEindpunt.getString("af.afhaalpuntStation");
                    stationNum = countPerPakketEindpunt.getInt("af.afhaalpuntID");
                    pakjesPerStation = countPerPakketEindpunt.getInt("pakjesPerStation");

                    tempStringX = Integer.toString(stationNum);
                    tempStringY = Integer.toString(pakjesPerStation);
                    tellert++;
                    for (x = x; x < tellert; x++) {

                        this.resultArr[x] = new ArrayList<>();
                        this.resultArr[x].add(naamAfhaalpunt);//,tempStringX,tempStringY);
                        this.resultArr[x].add(tempStringX);
                        this.resultArr[x].add(tempStringY);
                        System.out.println(this.resultArr[x]);
                    }
                    
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}
