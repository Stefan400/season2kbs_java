/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import tztdatabase.Database;

/**
 * @author nickh
 */
public class LoginDialog extends JDialog implements ActionListener {
    private boolean checked;
    private JTextField tfUsername, tfPassword;
    private JLabel lbUsername, lbPassword;
    private JButton btLogin, btClose;
    public String username;

    public LoginDialog() {
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.setModal(true);
        this.setLayout(new GridLayout(3, 2));
        this.setSize(320, 160);
        this.makeComponents();
        this.addComponents();

        if (checked) {
            this.setVisible(false);
        } else {
            this.setVisible(true);
        }
    }

    private void makeComponents() {
        tfUsername = new JTextField();
        tfPassword = new JPasswordField();
        lbUsername = new JLabel("Gebruikersnaam: ");
        lbPassword = new JLabel("Wachtwoord: ");
        btLogin = new JButton("Login");
        btClose = new JButton("Sluiten");

        btLogin.addActionListener(this);
        btClose.addActionListener(this);
    }

    private void addComponents() {
        this.add(lbUsername);
        this.add(tfUsername);
        this.add(lbPassword);
        this.add(tfPassword);
        this.add(btClose);
        this.add(btLogin);
    }

    private boolean loggedIn(String username, String password) {
        try {
            Database db = new Database();

            ResultSet account = db.selectSet("SELECT * FROM account WHERE accountGebruikersnaam ='" + username + "' AND accountUserlevel = 2;");
            while (account.next()) {
                String passwordInDb = account.getString("accountSalt");
                if (password.equals(passwordInDb)) {
                    this.username = account.getString("accountGebruikersnaam");
                    return true;
                } else {
                    return false;
                }
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return false;
    }

    public boolean getChecked() {
        return this.checked;
    }

    public void setChecked(boolean newChecked) {
        this.checked = newChecked;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        Component c = (Component) e.getSource();
        if (o == btClose) {
            this.setVisible(false);
            System.exit(0);
        }
        if (o == btLogin) {
            if (this.loggedIn(this.tfUsername.getText(), this.tfPassword.getText())) {
                this.checked = true;
                this.setVisible(false);
            } else {
                JOptionPane.showMessageDialog(this, "Gebruikersnaam of wachtwoord incorrect!");
            }
        }
    }
}
