/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;

import java.awt.*;
import java.sql.*;
import javax.swing.*;

import tztdatabase.Database;
import tztincludes.WrapLayout;

/**
 * @author nickh
 */
public class GebruikerPage extends JPanel {
    public GebruikerPage() {
        Database db = new Database();
        this.setBackground(new Color(200, 200, 200));
        this.setLayout(new WrapLayout());

        try {
            ResultSet allAccounts = db.selectSet("SELECT * FROM account ORDER BY accountGebruikersnaam ASC");
            while (allAccounts.next()) {
                final String accountID = allAccounts.getString("accountID");
                GebruikerPageLabel gpl = new GebruikerPageLabel(accountID);
                add(gpl);
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }
}
