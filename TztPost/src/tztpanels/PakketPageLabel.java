/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;


import java.sql.*;
import java.awt.*;
import java.awt.event.*;

import tztdatabase.*;

import javax.swing.*;

public class PakketPageLabel extends JLabel implements ActionListener {
    private String nummer, van, naar;
    private JButton button, traceButton;
    private Database db;

    public PakketPageLabel(int pakketID) {
        db = new Database();
        String id = Integer.toString(pakketID);

        ResultSet pakket = db.selectSet("SELECT * FROM pakket WHERE pakketID=" + id);
        try {
            while (pakket.next()) {
                nummer = pakket.getString("pakketID");

                String vanSt = this.db.selectSingle("SELECT afhaalpuntStation FROM afhaalpunt WHERE afhaalpuntID = " + pakket.getString("pakketBeginpunt"), "afhaalpuntStation");
                String naarSt = this.db.selectSingle("SELECT afhaalpuntStation FROM afhaalpunt WHERE afhaalpuntID = " + pakket.getString("pakketEindpunt"), "afhaalpuntStation");

                van = vanSt;
                naar = naarSt;
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }

        //Vormgeving label
        this.setPreferredSize(new Dimension(550, 100));
        this.setOpaque(true);
        this.setBackground(new Color(150, 150, 150));
        this.setLayout(new GridLayout(4, 2));

        button = new JButton("Details");
        button.addActionListener(this);
        button.setBackground(new Color(50, 255, 50));

        traceButton = new JButton("Locatiegegevens");
        traceButton.addActionListener(this);
        traceButton.setBackground(new Color(50, 255, 50));

        //Toevoegingen
        this.add(new JLabel("Pakketnummer: " + nummer));
        this.add(new JLabel());
        this.add(new JLabel("Van: " + van));
        this.add(new JLabel());
        this.add(new JLabel("Naar: " + naar));
        this.add(new JLabel());
        this.add(button);
        this.add(traceButton);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();

        if (o == button) {
            PakketPageDetail ppd = new PakketPageDetail(Integer.parseInt(this.nummer));
            ppd.setVisible(true);
        } else {
            PakketPageTracer ppt = new PakketPageTracer(Integer.parseInt(this.nummer));
            ppt.setVisible(true);
        }
    }
}
