package tztpanels;

import tztdatabase.Database;
import tztincludes.WrapLayout;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.awt.Graphics;


/**
 * countPerPakketEindpunt = this.db.selectSet("SELECT af.afhaalpuntStation, af.afhaalpuntID, COUNT( * ) AS pakjesPerStation\n" +
 * "FROM afhaalpunt AS af\n" +
 * "JOIN pakket AS p ON af.afhaalpuntID = p.pakketEindpunt\n" +
 * "GROUP BY af.afhaalpuntStation ");
 */
public class MainPageTekenen extends JPanel implements ActionListener {

    private Graphics g;
    private int countert = 0;
    private int countertMax = 7;
    private JComboBox stationKeuze = new JComboBox();
    private Database db = new Database();
    private int tellert;
    private ArrayList<String>[] resultArr;
    private String naamAfhaalpunt, afhaalpunt, pakjesPerStationString;
    private static int i = 150; // bepaald de waarde waarmee wordt vermededigd vuldigd
    private boolean boven500 = false;
    private int lengte, index;
    private JComboBox comboBox = new JComboBox();
    private BufferedImage stations;
    private JButton stationVolgorde, keuzeKnop;
    private Font font;
    private String[] description;

    public MainPageTekenen(ArrayList<String>[] resultArr, int tellert, int index) {//waar hij mag tekenen wordt al gezet de gridlayout
        this.resultArr = resultArr;
        this.tellert = tellert;
        this.index = index;
        font = new Font("Verdana", Font.BOLD, 12);
        setFont(font);
        description = new String[]{};

        setLayout(new FlowLayout(FlowLayout.RIGHT));
        if (index == 1) {
            repaint();
            stationVolgorde = new JButton("volgende statestieken");
            stationVolgorde.addActionListener(this);
            keuzeKnop = new JButton("statestieken naar keuze ");
            keuzeKnop.addActionListener(this);

            add(stationVolgorde);
            add(stationKeuze);
            add(keuzeKnop);

        }


        System.out.println("Tekenen");
        setBackground(new Color(189, 203, 154));
        //setSize(600, 550);
        setBorder(BorderFactory.createLineBorder(new Color(160, 160, 138)));
        //jscrollpane


    }


    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.g = g;
        // wanneer alle stations in de database staan
        int y = 0;
/*
        System.out.println(countert);

        for (int paintTellert = 0; paintTellert < tellert; paintTellert++) {

            naamAfhaalpunt = this.resultArr[paintTellert].get(0); //haalt de array op van de verschillende stations
            afhaalpunt = this.resultArr[paintTellert].get(1);
            pakjesPerStationString = this.resultArr[paintTellert].get(2); // aantal
        }
        int pakjes = Integer.parseInt(pakjesPerStationString); // hoeveelheid pakketjes

        if (y == 0) {
            y = 60;
        } else {
            y = 2 * y; // naar beneden

        }
        g.drawString(afhaalpunt + " " + naamAfhaalpunt + " heeft " + pakjes + " pakketten ontvangen", 20, y);



        System.out.println(afhaalpunt+naamAfhaalpunt+pakjes+"");

        String medewerkersString = "Locatie " +description[countert]+ " pakketjes aantal "+pakjes;
*/

/*
   */


        int H = 19;
        ArrayList<Integer> c = new ArrayList<Integer>();
        if (index == 0)

        {
            index0(); // pakketen per station in totaal
        } else if (index == 1)

        {


            for (int paintTellert = 0; paintTellert < tellert; paintTellert++) {
                {

                    naamAfhaalpunt = this.resultArr[paintTellert].get(0);

                   // System.out.println(naamAfhaalpunt);
                }


            int adamCentraal = 0;//1 - 1;
            int rdamCentraal = 2 - 1;
            int gouda = 3 - 1;
            int utrecht = 4 - 1;
            int woerden = 5 - 1;
            int leiden = 6 - 1;
            int haarlem = 7 - 1;

            index1(); // verzenders vs werknemers

            if (countert == haarlem) {//Haarlem

                g.setColor(new Color(255, 32, 1));
                g.fillOval(90, 40, H, H);

                naamAfhaalpunt = this.resultArr[6].get(0); //haalt de array op van de verschillende stations
                afhaalpunt = this.resultArr[6].get(1);
                pakjesPerStationString = this.resultArr[6].get(2); // aantal


            } else if (countert == adamCentraal) {//Amsterdam Centraal
                g.setColor(new Color(36, 244, 0));
                g.fillOval(230, 40, H, H);
               // g.drawString("kiosknummer:",375,80);



            } else if (countert == leiden) { //leiden
                g.setColor(new Color(255, 234, 32));
                g.fillOval(60, 183, H, H);

                naamAfhaalpunt = this.resultArr[0].get(0); //haalt de array op van de verschillende stations
                afhaalpunt = this.resultArr[0].get(1);
                pakjesPerStationString = this.resultArr[0].get(2); // aantal
                System.out.println(naamAfhaalpunt + "< naam " + afhaalpunt + "<<<< afhalenpunt" + pakjesPerStationString + "   " + tellert);


            } else if (countert == utrecht) {//Utrecht
                g.setColor(new Color(0, 130, 212));
                g.fillOval(292, 227, H, H);

            } else if (countert == woerden) {// Woerden
                g.setColor(new Color(244, 0, 208));
                g.fillOval(200, 220, H, H);

            } else if (countert == gouda) { // Gouda
                g.setColor(new Color(244, 156, 0));
                g.fillOval(160, 260, H, H);

            } else if (countert == rdamCentraal) { // Rotterdam
                g.setColor(new Color(0, 244, 229));
                g.fillOval(70, 375, H, H);
            }
        }
    }

}


    public void index0() {
        int maxBalk;
        int grootsteBalk = 0;
        int y = 0;

        for (int paintTellert = 0; paintTellert < tellert; paintTellert++) {
            for (int s = 0; s < tellert; s++) {
                lengte = 500;
                int q = 0;
                pakjesPerStationString = this.resultArr[s].get(2); // aantal
                int pakjes = Integer.parseInt(pakjesPerStationString); // hoeveelheid pakketjes

                maxBalk = i * pakjes;
                if (maxBalk > 500) {
                    boven500 = false;
                }
                while (boven500 == false) { // i wordt aan gepast daarna wordt aantal pakketten * i gedaan.
                    i--;
                    maxBalk = i * pakjes;
                    if (maxBalk < 500) { // bepalen wat maximaal aantal pixels zijn om vervolgens in te kleuren
                        boven500 = true;
                    }

                }
            }

            g.setColor(new Color(0, 0, 0));


            naamAfhaalpunt = this.resultArr[paintTellert].get(0); //haalt de array op van de verschillende stations
            afhaalpunt = this.resultArr[paintTellert].get(1);
            pakjesPerStationString = this.resultArr[paintTellert].get(2); // aantal

            int pakjes = Integer.parseInt(pakjesPerStationString); // hoeveelheid pakketjes
            int baseLineAantalPakketjes = pakjes * i; //Koploper maakt de grootte van de tabel

            if (y == 0) {
                y = 20;
            } else {
                y = 25 + y; // naar beneden

            }


            int maxLengte = 500;
            int status = pakjes * i;

            g.drawRect(20, y, maxLengte, 5); // eerste vel
            g.drawString(afhaalpunt + " " + naamAfhaalpunt + " heeft " + pakjes + " pakketten ontvangen", 20, y);

            g.setColor(new Color(9, 231, 0)); // set color
            g.drawRect(20, y, status, 5); // setwaarde

            int opvullert = 0;
            for (opvullert = opvullert; opvullert < status; opvullert++) {
                if (opvullert % 2 == 1) {
                    g.setColor(new Color(2, 2, 2)); // set color
                    g.drawLine(20 + opvullert, 20, 20 + opvullert, 25);
                    opvullert++;
                } else {

                    g.setColor(new Color(1, 14, 225)); // set color
                    g.drawLine(20 + opvullert, y, 20 + opvullert, 5 + y);
                    opvullert++;
                }
            }
        }
    }

    private void index1() {
        try {
            stations = ImageIO.read(new File("src/tztpanels/tracks.jpg"));

        } catch (IOException ex) {
            System.out.println("Plaatje niet gevonden.");
        }



        setBackground(new Color(154, 171, 123));
        g.drawImage(stations, 0, 0, null);

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();

        if (o == stationVolgorde) {
            if (countert == countertMax) {
                countert = 0;
            }
            repaint();
            countert++;
        }
        if (o == keuzeKnop) {
            int x = 0;
            System.out.println(countert);

            countert = (stationKeuze.getSelectedIndex());
            System.out.println(countert);
            repaint();
        }

    }
}
