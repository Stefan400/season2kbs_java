package tztpanels;

import java.awt.event.*;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;

import tztdatabase.*;

import javax.swing.*;


public class MainPageDetailsRecords extends JDialog implements ActionListener {
    private Database db = new Database();
    public JTextField nummer, formaat, gewicht, van, naar, afzender;
    private JDialog dia;
    public JButton edit, close;
    private JLabel tempLabel;
    private JLabel containert;
    private int welkeInhoud;
    private ResultSet queryResult;

    public MainPageDetailsRecords(JDialog dia, int welkeInhoud) {
        this.setModal(true);
        this.welkeInhoud = welkeInhoud;
        this.dia = dia;
        this.welkeInhoud = welkeInhoud;

        containert = new JLabel();
        containert.setOpaque(true);
        containert.setBackground(Color.blue);
        containert.setLayout(new FlowLayout());


        /*
        0= allepakketten
        1= max gewicht pakket
        2= min gewicht pakket
        3= pakket in m3
        4= langste pakket
         */

        if (welkeInhoud == 0) {
            System.out.println("Inhoud " + welkeInhoud);
            this.setTitle("Alle pakketten");

            this.setSize(1000, 650);
            this.setLayout(new GridLayout(50, 3));

            //Set velden voor alle gebruikers
            alleGebruikers();

        } else {
            System.out.println("Inhoud " + welkeInhoud);
            this.setSize(1100, 250);
            this.setLayout(new GridLayout(2, 1));
            if (welkeInhoud == 1) {
                this.queryResult = this.db.selectSet("SELECT p.pakketID, p.pakketLengte, p.pakketBreedte, p.pakketHoogte, p.pakketGewicht, p.afzenderID, p.pakketVerzenddatum, p.pakketBeginpunt, p.pakketEindpunt, p.pakketOphalenVanHuis, p.ontvangerID FROM pakket AS p LEFT JOIN afzender AS a ON p.afzenderID = a.afzenderID ORDER BY `p`.`pakketGewicht` DESC LIMIT 1");
                maximaalSizePakket(this.queryResult);

            } else if (welkeInhoud == 2) {
                this.setTitle("pakket minimalle gewicht");
                this.queryResult = this.db.selectSet("SELECT p.pakketID, p.pakketLengte,  p.pakketBreedte, p.pakketHoogte, p.pakketGewicht, p.afzenderID, p.pakketVerzenddatum,  p.pakketBeginpunt, p.pakketEindpunt, p.pakketOphalenVanHuis, p.ontvangerID FROM pakket AS p LEFT JOIN afzender AS a ON p.afzenderID = a.afzenderID ORDER BY  `p`.`pakketGewicht` ASC LIMIT 1");
                minGewichtPakket(queryResult);
            } else if (welkeInhoud == 3) {
                this.setTitle("pakket in m3 ");
                this.queryResult = this.db.selectSet("SELECT ((p.pakketlengte*p.pakketBreedte*p.pakketHoogte)) as lbh, p.pakketID, p.pakketLengte, p.pakketBreedte, p.pakketHoogte, p.pakketGewicht, p.afzenderID, p.pakketVerzenddatum, p.pakketBeginpunt, p.pakketEindpunt, p.pakketOphalenVanHuis, p.ontvangerID FROM pakket AS p LEFT JOIN afzender AS a ON p.afzenderID = a.afzenderID ORDER BY `lbh` DESC LIMIT 1");

                minGewichtPakket(queryResult);
            } else if (welkeInhoud == 4) {
                this.setTitle("langste pakket");
                this.queryResult = this.db.selectSet("SELECT p.pakketID, p.pakketLengte,  p.pakketBreedte, p.pakketHoogte, p.pakketGewicht, p.afzenderID, p.pakketVerzenddatum,  p.pakketBeginpunt, p.pakketEindpunt, p.pakketOphalenVanHuis, p.ontvangerID FROM pakket AS p LEFT JOIN afzender AS a ON p.afzenderID = a.afzenderID ORDER BY  `p`.`pakketLengte` ASC LIMIT 1");
                langstePakket(queryResult);
            } else {
                System.out.println("Error: 404 = welkeInhoud is niet geselecteerd");
            }


        }
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();


        if (source == this.close) {
            this.setVisible(false);
        }

    }


    private void alleGebruikers() { //inhoud0
        nummer = new JTextField();
        formaat = new JTextField();
        gewicht = new JTextField();
        van = new JTextField();
        naar = new JTextField();
        afzender = new JTextField();
        edit = new JButton("Bewerken");
        close = new JButton("Sluiten");
        edit.addActionListener(this);
        close.addActionListener(this);

        /*
        SELECT p.pakketID, p.pakketLengte, p.pakketBreedte, p.pakketHoogte, max(p.pakketGewicht), p.afzenderID, p.pakketVerzenddatum, p.pakketBeginpunt, p.pakketEindpunt, p.pakketOphalenVanHuis, p.ontvangerID FROM pakket as p
LEFT JOIN afzender as a ON p.afzenderID = a.afzenderID

         */

        try {
            ResultSet pakketjes = this.db.selectSet("SELECT * FROM pakket LEFT JOIN afzender ON pakket.afzenderID=afzender.afzenderID ");

            while (pakketjes.next()) {
                nummer.setText(pakketjes.getString("pakketID"));
                formaat.setText(pakketjes.getString("pakketLengte") + "x" + pakketjes.getString("pakketBreedte") + "x" + pakketjes.getString("pakketHoogte"));
                gewicht.setText(pakketjes.getString("pakketGewicht"));

                afzender.setText(pakketjes.getString("afzender.afzenderAanhef") + " " + pakketjes.getString("afzender.afzenderAchternaam"));

                String vanSt = this.db.selectSingle("SELECT afhaalpuntStation FROM afhaalpunt WHERE afhaalpuntID = " + pakketjes.getString("pakketBeginpunt"), "afhaalpuntStation");

                String naarSt = this.db.selectSingle("SELECT afhaalpuntStation FROM afhaalpunt WHERE afhaalpuntID = " + pakketjes.getString("pakketEindpunt"), "afhaalpuntStation");

                van.setText(vanSt);
                naar.setText(naarSt);

                tempLabel = new JLabel("Pakket id " + nummer.getText() + " | format " + formaat.getText() + " | gewicht " + gewicht.getText() + " | afzender " + afzender.getText() + " | van " + van.getText() + " | naar " + naar.getText());

                setColors(tempLabel);
                this.add(tempLabel);


            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    private void maximaalSizePakket(ResultSet queryResult) { //inhoud 1

        try {
            while (queryResult.next()) {

                inhoudBepalen(queryResult);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


    private void minGewichtPakket(ResultSet containert) { //inhoud 2
        try {
            while (queryResult.next()) {

                inhoudBepalen(queryResult);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void langstePakket(ResultSet containert) { // inhoud 4
        try {
            while (queryResult.next()) {

                inhoudBepalen(queryResult);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void inhoudBepalen(ResultSet queryResult) {
        try {
            // tabel in Stringen gooien
            int pakketID = this.queryResult.getInt("p.pakketID");
            String pakketGewicht = this.queryResult.getString("p.pakketGewicht");
            String pakketLengte = this.queryResult.getString("p.pakketBreedte");
            String pakketHoogte = this.queryResult.getString("p.pakketHoogte");
            String pakketAfzender = this.queryResult.getString("p.pakketGewicht");
            String pakketVerzender = this.queryResult.getString("p.pakketGewicht");
            String pakketOntvanger = this.queryResult.getString("p.ontvangerID");
            JLabel lblTemp = new JLabel();
            JLabel infoTemp = new JLabel();

            //welke informatie is het weer geven
            if (welkeInhoud == 1) {
                infoTemp = new JLabel("Het maximale gewicht is " + pakketGewicht);
                lblTemp = new JLabel("pakketgegevens: id:" +
                        "" + pakketID + " gewicht: " + pakketGewicht + " in grammen | lengte" + pakketLengte + " in CM | hoogte  " + pakketHoogte + " in CM | afzender id" + pakketAfzender + " | verzender " + pakketVerzender + " | ontvanger " + pakketOntvanger);
                lblTemp.setBorder(BorderFactory.createLineBorder(Color.black));

                this.add(lblTemp);
            } else if (welkeInhoud == 2) {

                infoTemp = new JLabel("Het minimaal gewicht is " + pakketGewicht);
                lblTemp = new JLabel("pakketgegevens: id:" +
                        "" + pakketID + " gewicht: " + pakketGewicht + " in grammen | lengte" + pakketLengte + " in CM | hoogte  " + pakketHoogte + " in CM | afzender id" + pakketAfzender + " | verzender " + pakketVerzender + " | ontvanger " + pakketOntvanger);
            } else if (welkeInhoud == 3) {
                int pakketCm3 = this.queryResult.getInt("lbh");
                infoTemp = new JLabel("De M3 " + pakketCm3);
                lblTemp = new JLabel("pakketgegevens: id: " + "" + pakketID + " pakket in cm3 "+pakketCm3 +" gewicht: " + pakketGewicht + " in grammen | lengte" + pakketLengte + " in CM | hoogte  " + pakketHoogte + " in CM | afzender id" + pakketAfzender + " | verzender " + pakketVerzender + " | ontvanger " + pakketOntvanger);

            } else if (welkeInhoud == 4) {
                infoTemp = new JLabel("De maximale lengte is " + pakketLengte);
                lblTemp = new JLabel("pakketgegevens: id:" +
                        "" + pakketID + " gewicht: " + pakketGewicht + " in grammen | lengte" + pakketLengte + " in CM | hoogte  " + pakketHoogte + " in CM | afzender id" + pakketAfzender + " | verzender " + pakketVerzender + " | ontvanger " + pakketOntvanger);

            }

            setColors(infoTemp);
            setColors(lblTemp);

            this.add(infoTemp);
            this.add(lblTemp);


        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void setColors(JLabel x) {
        int greyTint = 150;
        x.setBorder(BorderFactory.createLineBorder(new Color(121, 218, 24)));
        x.setOpaque(true);
        x.setBackground(new Color(164, 244, 51));
/*
        edit.setBackground(new Color(50, 255, 50));
        close.setBackground(new Color(50, 255, 50));
  */
    }
}



