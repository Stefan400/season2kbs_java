/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import tztdatabase.Database;

/**
 *
 * @author nickh
 */
public class HrAfdelingPageDetail extends JDialog implements ActionListener{
    private String record;
    private JLabel aanhef, voornaam, tussenvoegsel, achternaam, telnr, email, adres, postcode, woonplaats, notities;
    private JTextField tfAanhef, tfVoornaam, tfTussenvoegsel, tfAchternaam, tfTelnr, tfEmail, tfAdres, tfPostcode, tfWoonplaats;
    private JTextArea tfNotities;
    private JButton editSave, close;
    
    Database db = new Database();
    
    public HrAfdelingPageDetail(String id){
        this.record = id;
        this.setTitle("Details: Sollicitant " + record);
        this.setModal(true);
        this.setSize(480, 600);
        this.setLayout(new GridLayout(11, 2));
        this.setComponents();
        this.setColors();
    }
    
    private void setComponents(){
        aanhef = new JLabel("Aanhef:");
        voornaam = new JLabel("Voornaam:");
        tussenvoegsel = new JLabel("tussenvoegsel:");
        achternaam = new JLabel("Achternaam:");
        telnr = new JLabel("Telefoonnummer:");
        email = new JLabel("E-mail:");
        adres = new JLabel("Adres:");
        postcode = new JLabel("Postcode:");
        woonplaats = new JLabel("Woonplaats:");
        notities = new JLabel("Notities:");

        aanhef.setOpaque(true);
        voornaam.setOpaque(true);
        tussenvoegsel.setOpaque(true);
        achternaam.setOpaque(true);
        telnr.setOpaque(true);
        email.setOpaque(true);
        adres.setOpaque(true);
        postcode.setOpaque(true);
        woonplaats.setOpaque(true);
        notities.setOpaque(true);

        tfAanhef = new JTextField();
        tfVoornaam = new JTextField();
        tfTussenvoegsel = new JTextField();
        tfAchternaam = new JTextField();
        tfTelnr = new JTextField();
        tfEmail = new JTextField();
        tfAdres = new JTextField();
        tfPostcode = new JTextField();
        tfWoonplaats = new JTextField();

        tfNotities = new JTextArea();
        JScrollPane scrNotities = new JScrollPane(tfNotities);
        scrNotities.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrNotities.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        editSave = new JButton("Bewerken");
        close = new JButton("Sluiten");

        editSave.addActionListener(this);
        close.addActionListener(this);
        this.fillUp(record);
        tfAanhef.setEditable(false);
        tfVoornaam.setEditable(false);
        tfTussenvoegsel.setEditable(false);
        tfAchternaam.setEditable(false);
        tfTelnr.setEditable(false);
        tfEmail.setEditable(false);
        tfAdres.setEditable(false);
        tfPostcode.setEditable(false);
        tfWoonplaats.setEditable(false);
        tfNotities.setEditable(false);

        this.add(aanhef);
        this.add(tfAanhef);
        this.add(voornaam);
        this.add(tfVoornaam);
        this.add(tussenvoegsel);
        this.add(tfTussenvoegsel);
        this.add(achternaam);
        this.add(tfAchternaam);
        this.add(telnr);
        this.add(tfTelnr);
        this.add(email);
        this.add(tfEmail);
        this.add(adres);
        this.add(tfAdres);
        this.add(postcode);
        this.add(tfPostcode);
        this.add(woonplaats);
        this.add(tfWoonplaats);
        this.add(notities);
        this.add(scrNotities);
        this.add(editSave);
        this.add(close);
    }
    
    private void fillUp(String id){
        try{
            ResultSet sollicitant = db.selectSet("SELECT * FROM sollicitant WHERE sollicitantID = " + this.record);
            while(sollicitant.next()){
                this.tfAanhef.setText(sollicitant.getString("sollicitantAanhef"));
                this.tfVoornaam.setText(sollicitant.getString("sollicitantVoornaam"));
                this.tfTussenvoegsel.setText(sollicitant.getString("sollicitantTussenvoegsel"));
                this.tfAchternaam.setText(sollicitant.getString("sollicitantAchternaam"));
                this.tfTelnr.setText(sollicitant.getString("sollicitantTelefoonnummer"));
                this.tfEmail.setText(sollicitant.getString("sollicitantEmail"));
                this.tfAdres.setText(sollicitant.getString("sollicitantAdres"));
                this.tfWoonplaats.setText(sollicitant.getString("sollicitantWoonplaats"));
                this.tfPostcode.setText(sollicitant.getString("sollicitantPostcode"));
                this.tfNotities.setText(sollicitant.getString("sollicitantNotities"));
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    private void setColors() {
        int greyTint = 150;
        aanhef.setBackground(new Color(greyTint, greyTint, greyTint));
        voornaam.setBackground(new Color(greyTint, greyTint, greyTint));
        tussenvoegsel.setBackground(new Color(greyTint, greyTint, greyTint));
        achternaam.setBackground(new Color(greyTint, greyTint, greyTint));
        telnr.setBackground(new Color(greyTint, greyTint, greyTint));
        email.setBackground(new Color(greyTint, greyTint, greyTint));
        adres.setBackground(new Color(greyTint, greyTint, greyTint));
        postcode.setBackground(new Color(greyTint, greyTint, greyTint));
        woonplaats.setBackground(new Color(greyTint, greyTint, greyTint));
        notities.setBackground(new Color(greyTint, greyTint, greyTint));
        editSave.setBackground(new Color(50, 255, 50));
        close.setBackground(new Color(50, 255, 50));
        this.repaint();
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source == editSave) {
            if (editSave.getText().equals("Bewerken")) {
                tfAanhef.setEditable(true);
                tfVoornaam.setEditable(true);
                tfTussenvoegsel.setEditable(true);
                tfAchternaam.setEditable(true);
                tfTelnr.setEditable(true);
                tfEmail.setEditable(true);
                tfAdres.setEditable(true);
                tfPostcode.setEditable(true);
                tfWoonplaats.setEditable(true);
                tfNotities.setEditable(true);
                editSave.setText("Opslaan");
                this.repaint();
            } else {
                editSave.setText("Bewerken");
                tfAanhef.setEditable(false);
                tfVoornaam.setEditable(false);
                tfTussenvoegsel.setEditable(false);
                tfAchternaam.setEditable(false);
                tfTelnr.setEditable(false);
                tfEmail.setEditable(false);
                tfAdres.setEditable(false);
                tfPostcode.setEditable(false);
                tfWoonplaats.setEditable(false);
                tfNotities.setEditable(false);
                
                db.update("UPDATE sollicitant SET "
                        + " sollicitantAanhef = '" + tfAanhef.getText()
                        + "', sollicitantVoornaam = '" + tfVoornaam.getText()
                        + "', sollicitantTussenvoegsel = '" + tfTussenvoegsel.getText()
                        + "', sollicitantAchternaam = '" + tfAchternaam.getText()
                        + "', sollicitantTelefoonnummer = '" + tfTelnr.getText()
                        + "', sollicitantEmail = '" + tfEmail.getText()
                        + "', sollicitantAdres = '" + tfAdres.getText()
                        + "', sollicitantPostcode = '" + tfPostcode.getText()
                        + "', sollicitantWoonplaats = '" + tfWoonplaats.getText()
                        + "', sollicitantNotities = '" + tfNotities.getText()
                        + "' WHERE sollicitantID = " + this.record);
            }
        }
        else{
            this.setVisible(false);
        }
    }
}
