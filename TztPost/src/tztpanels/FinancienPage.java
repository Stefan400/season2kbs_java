package tztpanels;

import java.awt.Color;
import java.sql.*;

import javax.swing.*;
import tztdatabase.Database;
import tztincludes.*;

public class FinancienPage extends JPanel {
    public FinancienPage() {
        this.setBackground(new Color(200, 200, 200));
        this.setLayout(new WrapLayout());
        Database db = new Database();
        
        ResultSet allWerknemers = db.selectSet("SELECT * FROM account WHERE werknemerID > 0 AND afzenderID IS NULL");
        try{
            while(allWerknemers.next()){
                final String accountID = allWerknemers.getString("accountID");
                FinancienPageLabel fpl = new FinancienPageLabel(accountID);
                this.add(fpl);
            }
        }
        catch(SQLException sqle){
            sqle.printStackTrace();
        }
    }
}
