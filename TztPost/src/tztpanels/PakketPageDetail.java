/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;

import java.awt.event.*;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import tztdatabase.*;

import javax.swing.*;

public class PakketPageDetail extends JDialog implements ActionListener {
    private Database db = new Database();
    private int id;
    public JTextField nummer, formaat, gewicht, van, naar, afzender;
    public JLabel pcLabel, fmLabel, gwLabel, nrLabel, vnLabel, acLabel;
    public JButton edit, close;

    public PakketPageDetail(int pakketID) {
        this.setModal(true);
        this.setTitle("Details: Pakket " + pakketID);
        this.setSize(480, 600);
        this.setLayout(new GridLayout(7, 2));
        this.id = pakketID;
        //Set velden
        setComponents();
        setColors();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        if (source == this.close) {
            this.setVisible(false);
        } else if (source == this.edit) {
            if (this.edit.getText().equals("Bewerken")) {
                formaat.setEditable(true);
                gewicht.setEditable(true);
                van.setEditable(false);
                naar.setEditable(false);
                edit.setText("Opslaan");
            } else {
                formaat.setEditable(false);
                gewicht.setEditable(false);
                van.setEditable(false);
                naar.setEditable(false);

                String sqlAddition = "'" + formaat.getText() + "'," + "" + gewicht.getText() + "," + "'" + van.getText() + "'," + "'" + naar.getText() + "'";
                String query = "UPDATE pakket SET "
                        + "pakketLengte=" + formaat.getText().substring(0, 2) +
                        ", pakketBreedte=" + formaat.getText().substring(3, 5) +
                        ", pakketHoogte=" + formaat.getText().substring(6, 8) +
                        ", pakketGewicht=" + gewicht.getText() +
                        " WHERE pakketID=" + this.id + ";";
                System.out.println(query);
                this.db.update(query);

                edit.setText("Bewerken");
            }
        }
    }

    private void setComponents() {
        nummer = new JTextField();
        formaat = new JTextField();
        gewicht = new JTextField();
        van = new JTextField();
        naar = new JTextField();
        afzender = new JTextField();
        edit = new JButton("Bewerken");
        close = new JButton("Sluiten");
        edit.addActionListener(this);
        close.addActionListener(this);

        try {
            ResultSet pakket = this.db.selectSet("SELECT * FROM pakket "
                    + "LEFT JOIN afzender ON pakket.afzenderID=afzender.afzenderID "
                    + "WHERE pakketID = '" + this.id + "';");
            while (pakket.next()) {
                nummer.setText(pakket.getString("pakketID"));
                formaat.setText(pakket.getString("pakketLengte") + "x" + pakket.getString("pakketBreedte") + "x" + pakket.getString("pakketHoogte"));
                gewicht.setText(pakket.getString("pakketGewicht"));
                afzender.setText(pakket.getString("afzender.afzenderAanhef") + " " + pakket.getString("afzender.afzenderAchternaam"));

                String vanSt = this.db.selectSingle("SELECT afhaalpuntStation FROM afhaalpunt WHERE afhaalpuntID = " + pakket.getString("pakketBeginpunt"), "afhaalpuntStation");
                String naarSt = this.db.selectSingle("SELECT afhaalpuntStation FROM afhaalpunt WHERE afhaalpuntID = " + pakket.getString("pakketEindpunt"), "afhaalpuntStation");

                van.setText(vanSt);
                naar.setText(naarSt);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        nummer.setEditable(false);
        formaat.setEditable(false);
        gewicht.setEditable(false);
        van.setEditable(false);
        naar.setEditable(false);
        afzender.setEditable(false);

        pcLabel = new JLabel("Pakketcode: ");
        fmLabel = new JLabel("Formaat (LxBxH in cm): ");
        gwLabel = new JLabel("Gewicht (kg): ");
        vnLabel = new JLabel("Beginlocatie: ");
        nrLabel = new JLabel("Eindlocatie: ");
        acLabel = new JLabel("Afzender: ");

        this.add(pcLabel);
        this.add(this.nummer);
        this.add(fmLabel);
        this.add(this.formaat);
        this.add(gwLabel);
        this.add(this.gewicht);
        this.add(vnLabel);
        this.add(this.van);
        this.add(nrLabel);
        this.add(this.naar);
        this.add(acLabel);
        this.add(this.afzender);
        this.add(edit);
        this.add(close);
    }

    private void setColors() {
        int greyTint = 150;

        pcLabel.setOpaque(true);
        fmLabel.setOpaque(true);
        gwLabel.setOpaque(true);
        vnLabel.setOpaque(true);
        nrLabel.setOpaque(true);
        acLabel.setOpaque(true);

        pcLabel.setBackground(new Color(greyTint, greyTint, greyTint));
        fmLabel.setBackground(new Color(greyTint, greyTint, greyTint));
        gwLabel.setBackground(new Color(greyTint, greyTint, greyTint));
        vnLabel.setBackground(new Color(greyTint, greyTint, greyTint));
        nrLabel.setBackground(new Color(greyTint, greyTint, greyTint));
        acLabel.setBackground(new Color(greyTint, greyTint, greyTint));

        edit.setBackground(new Color(50, 255, 50));
        close.setBackground(new Color(50, 255, 50));
    }
}
