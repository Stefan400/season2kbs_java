/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;

import java.awt.Color;
import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import tztdatabase.Database;

/**
 *
 * @author nickh
 */
public class HrAfdelingPageLabel extends JLabel implements ActionListener{
    private String id, voornaam, tussenv, achternaam;
    private JLabel lbv, lbt, lba;
    private JButton details, maakAccount;
    
    public HrAfdelingPageLabel(String id){
        this.setPreferredSize(new Dimension(550, 100));
        this.setOpaque(true);
        this.setBackground(new Color(150, 150, 150));
        this.setLayout(new GridLayout(3, 2));
        this.id = id;
        
        Database db = new Database();
        ResultSet sollicitant = db.selectSet("SELECT * FROM sollicitant WHERE sollicitantID = " + this.id);
        try{
            while(sollicitant.next()){
                this.voornaam = sollicitant.getString("sollicitantVoornaam");
                this.tussenv  = sollicitant.getString("sollicitantTussenvoegsel");
                this.achternaam = sollicitant.getString("sollicitantAchternaam");
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        
        details = new JButton("Details");
        maakAccount = new JButton("Maak account");
        details.addActionListener(this);
        maakAccount.addActionListener(this);
        details.setBackground(new Color(50,255,50));
        maakAccount.setBackground(new Color(50,255,50));
        
        if(this.tussenv == null){
            this.tussenv = "";
        }
        
        this.add(new JLabel(this.tussenv + " " + this.achternaam + ", " + this.voornaam));
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(new JLabel());
        this.add(details);
        this.add(maakAccount);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if(o == details){
            HrAfdelingPageDetail hpd = new HrAfdelingPageDetail(this.id);
            hpd.setVisible(true);
        }
        if(o == maakAccount){
            HrAfdelingPageAccount hpa = new HrAfdelingPageAccount(this.id);
            hpa.setVisible(true);
        }
        //To change body of generated methods, choose Tools | Templates.
    }
}
