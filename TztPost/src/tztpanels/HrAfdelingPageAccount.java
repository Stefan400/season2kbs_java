/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
import tztdatabase.*;
import tztincludes.BCrypt;
import tztincludes.HashMemory;
/**
 *
 * @author nickh
 */
public class HrAfdelingPageAccount extends JDialog implements ActionListener{
    private String id, naam;
    private JLabel lbUsername, lbPassword, lbSollicitantnr, lbSollicitantnaam, sollNr, sollNaam;
    private JTextField username, password;
    private JButton aanmaken, sluiten;
    private Database db;
    
    public HrAfdelingPageAccount(String id){
        this.db = new Database();
        this.id = id;
        this.setSize(320,240);
        this.setTitle("Maak account aan: Sollicitant " + id);
        this.setModal(true);
        this.setLayout(new GridLayout(4,2));
        this.setComponents();
        this.setColors();
    }

    private void setComponents(){
        username = new JTextField();
        password = new JTextField();
        
        lbUsername = new JLabel("Gebruikersnaam: ");
        lbPassword = new JLabel("Wachtwoord: ");
        sollNr     = new JLabel("Sollicitantnummer: ");
        sollNaam   = new JLabel("Naam sollicitant: ");
        
        lbUsername.setOpaque(true);
        lbPassword.setOpaque(true);
        sollNr.setOpaque(true);
        sollNaam.setOpaque(true);
        
        aanmaken   = new JButton("Account aanmaken");
        sluiten    = new JButton("Sluiten");
        
        aanmaken.addActionListener(this);
        sluiten.addActionListener(this);
        
        try{
            ResultSet sollicitant = db.selectSet("SELECT * FROM sollicitant WHERE sollicitantID=" + id);
            while(sollicitant.next()){
                lbSollicitantnr = new JLabel(id);
                
                String tussenvoegsel = sollicitant.getString("sollicitantTussenvoegsel");
                if(tussenvoegsel == null){
                    tussenvoegsel = "";
                }
                
                lbSollicitantnaam = new JLabel(sollicitant.getString("sollicitantVoornaam") + " " + tussenvoegsel + " " + sollicitant.getString("sollicitantAchternaam"));
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        
        this.add(sollNr);
        this.add(lbSollicitantnr);
        this.add(sollNaam);
        this.add(lbSollicitantnaam);
        this.add(lbUsername);
        this.add(username);
        this.add(aanmaken);
        this.add(sluiten);
    }
    
    private void setColors(){
        int greyTint = 150;
        sollNr.setBackground(new Color(greyTint,greyTint,greyTint));
        sollNaam.setBackground(new Color(greyTint,greyTint,greyTint));
        lbUsername.setBackground(new Color(greyTint,greyTint,greyTint));
        lbPassword.setBackground(new Color(greyTint,greyTint,greyTint));
        aanmaken.setBackground(new Color(50, 255, 50));
        sluiten.setBackground(new Color(50, 255, 50));
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Object o = e.getSource();
        if(o == sluiten){
            this.setVisible(false);
        }
        if(o == aanmaken){
            //Eerst medewerker maken, daarna een account aanmaken
            db.insert("INSERT INTO werknemer (sollicitantID, functieID, werknemerSalaris, werknemerPunten) VALUES (" + this.id + ", 1, 0.00, 0);");
            
            //Verkrijgen werknemerID
            String werknemerID = db.selectSingle("SELECT werknemerID FROM werknemer WHERE sollicitantID = " + this.id, "werknemerID");
            
            db.insert("INSERT INTO account(accountGebruikersnaam, accountWachtwoord, accountSalt, accountUserlevel, werknemerID, afzenderID, accountLoginpogingen) VALUES ('" + this.username.getText() + "', '$2y$10$weqR7KSUIUWzDUT45AJJceWwsAY5vqb24/1cEwCR/.IO6EWCtxJdC','weqR7KSUIUWzDUT45AJJcsTo3Rx95DERw2EG', 1," + werknemerID + ", NULL , 0);");
            this.setVisible(false);
        }
    }
}
