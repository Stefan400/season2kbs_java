/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import tztdatabase.Database;

/**
 *
 * @author nickh
 */
public class FinancienPageLabel extends JPanel implements ActionListener{
    private String nummer, naam, functie, salaris, wID;
    private JLabel lbNaam, lbFunctie, lbSalaris;
    private JButton button;
    private Database db;
    
    public FinancienPageLabel(String id){
        this.setPreferredSize(new Dimension(550, 100));
        this.setOpaque(true);
        this.setBackground(new Color(150, 150, 150));
        this.setLayout(new GridLayout(4, 1));
        this.nummer = id;
        
        db = new Database();
        ResultSet account = db.selectSet("SELECT * FROM account WHERE accountID = " + id );
        try {
            while(account.next()){
                ResultSet werknemer = db.selectSet("SELECT * FROM werknemer wn LEFT JOIN sollicitant slct ON wn.sollicitantID = slct.sollicitantID LEFT JOIN functie f ON wn.functieID = f.functieID WHERE wn.werknemerID = " + account.getString("werknemerID"));
                
                while(werknemer.next()){
                    this.wID = werknemer.getString("werknemerID");
                    
                    String tussenvoegsel = werknemer.getString("sollicitantTussenvoegsel");
                    if(tussenvoegsel == null){
                        tussenvoegsel = "";
                    }
                    this.naam = werknemer.getString("sollicitantVoornaam") + " " + tussenvoegsel + " " + werknemer.getString("sollicitantAchternaam");
                    this.functie = werknemer.getString("functieOmschrijving");
                    this.salaris = werknemer.getString("werknemerSalaris");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(FinancienPageLabel.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        button = new JButton("Keer salaris uit");
        button.addActionListener(this);
        button.setBackground(new Color(50,255,50));
        
        this.add(new JLabel(naam));
        this.add(new JLabel(functie));
        this.add(new JLabel(salaris));
        this.add(button);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(functie.equals("Bezorger")){
            db.update("UPDATE werknemer SET werknemerSalaris = 0.00 WHERE werknemerID = " + this.wID);
            JOptionPane.showMessageDialog(this, "Salaris uitgekeerd. Salaris teruggezet naar 0");
        }
        else{
            JOptionPane.showMessageDialog(this, "Salaris uitgekeerd.");
        }
    }
}
