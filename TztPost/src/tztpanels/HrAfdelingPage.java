package tztpanels;

import javax.swing.*;
import java.awt.*;
import java.sql.*;
import tztdatabase.Database;
import tztincludes.*;

public class HrAfdelingPage extends JPanel {
    public HrAfdelingPage() {
        this.setBackground(new Color(200, 200, 200));
        this.setLayout(new WrapLayout());
        Database db = new Database();
        
        ResultSet alleSollicitanten = db.selectSet("SELECT * FROM sollicitant WHERE sollicitantID NOT IN(SELECT sollicitantID FROM werknemer)ORDER BY sollicitantAchternaam ASC" );
        try{
            while(alleSollicitanten.next()){
                final String ID = alleSollicitanten.getString("sollicitantID");
                HrAfdelingPageLabel hpl = new HrAfdelingPageLabel(ID);
                this.add(hpl);
            }
        }
        catch(SQLException sqle){
            sqle.printStackTrace();
        }
    }
}
