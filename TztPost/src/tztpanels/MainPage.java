/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;


import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.time.LocalDate;
import java.util.Date;
import javax.swing.JDialog;

import tztdatabase.*;


public class MainPage extends JPanel implements ActionListener {

    private LocalDate vandaag;
    private JLabel paginaKopText, containert, eersteRow;
    private JPanel footert;
    private JLabel tempLabel;
    private Database db;
    private JButton button = new JButton("details");
    private int count = 0;
    private JComboBox comboBox = new JComboBox();
    private JButton tekenDialoogStationStatus, tekenDialoogDagelijksPakket;
    private int index;

    private LineBorder line = new LineBorder(new Color(0, 38, 0), 1, true); // color, thickness, rounded

    private boolean loggedIn = false;

    public MainPage() {
        db = new Database();
        setLayout(new BorderLayout());
        add(paginaKop(paginaKopText), BorderLayout.NORTH);
        add(paginaContent(containert), BorderLayout.CENTER);
        add(paginaFooter(footert), BorderLayout.SOUTH);


    }

    public JComponent paginaKop(JLabel paginaKopText) {
        vandaag = LocalDate.now();
        this.paginaKopText = new JLabel("Home - dashbord " + vandaag, JLabel.CENTER);
        this.paginaKopText.setFont(new Font("Courier", Font.BOLD, 24));
        return this.paginaKopText;
    }

    public JComponent paginaContent(JLabel containert) {
        this.containert = containert;

        //Container style
        containert = new JLabel();
        containert.setOpaque(true);
        containert.setBackground(new Color(197, 195, 161));
        containert.setLayout(new GridLayout(3, 1));
        containert.setSize(400, 400);
        containert.setBorder(line);
        add(containert, BorderLayout.CENTER);

        //totaalAantalPakketten 1 row
        eersteRowTotaalPakketten(containert);

        //recordsNuActief 2 row
        tweedeRowVariableRecords(containert);

        //tekenen 3 row
        derdeRowTabbellen(containert);

        return containert;
    }

    private JComponent derdeRowTabbellen(JLabel containert) {
        JLabel containterContainert = new JLabel(); //3e container row
        setColors(containterContainert);
        containterContainert.setLayout(new GridLayout(4, 2));


        //3e container 1e row
        tekenDialoogStationStatus = new JButton("Dialoog totaal pakjes per station.");
        tekenDialoogStationStatus.addActionListener(this);
        containterContainert.add(tekenDialoogStationStatus);
        containert.add(containterContainert);

        //3e container 2e row
        tekenDialoogDagelijksPakket = new JButton("Dialoog pakjes per dag.");
        tekenDialoogDagelijksPakket.addActionListener(this);
        containterContainert.add(tekenDialoogDagelijksPakket);
        containert.add(containterContainert);


        return containert;
    }

    private JComponent eersteRowTotaalPakketten(JLabel containert) {
        eersteRow = new JLabel();
        eersteRow.setOpaque(true);
        eersteRow.setBackground(new Color(165, 173, 150));
        eersteRow.setLayout(new GridLayout(4, 2));
        //kopje
        JLabel tittle = new JLabel("aangemelde pakketten statestieken");
        tittle.setOpaque(true);
        tittle.setBackground(new Color(28, 239, 30));

        try {
            ResultSet pakket = db.selectSet("SELECT count(pakketID) as aantalPakketten FROM pakket");
            while (pakket.next()) {
                //pakket collom
                String x = pakket.getString("aantalPakketten");
                // maakt een niewe JLabel met de query
                tempLabel = new JLabel("Totaal aantal aangemelde pakketten " + x);

                //voegt de JLabel toe aan de andere JLabel
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }

        JLabel eersteLabel = new JLabel();
        dropdownList(eersteLabel); //functie dropdown
        button.addActionListener(this);
        //add alle JLabels aan het hoofd label
        eersteRow.add(tittle); // tittel
        eersteRow.add(tempLabel); // container eerste grid
        eersteRow.add(eersteLabel);

        containert.add(eersteRow);

        return containert;
    }

    private void dropdownList(JLabel eersteRow) {
        String[] description = {"Alle pakketten", "Max gewicht pakket", "Min gewicht pakket", "Pakket in cm3", "Langste pakket"};

        for (int i = 0; i < 5; i++) // for om elke optie aan te geven
            comboBox.addItem(description[count++]);

        eersteRow.setLayout(new FlowLayout());
        eersteRow.add(comboBox);
        eersteRow.add(button);

    }


    public JComponent allePakketten(JDialog dia) {
        JButton allePakkettenButton = new JButton("Alle pakketten");
        allePakkettenButton.addActionListener(this);
        eersteRow.add(allePakkettenButton);

        return eersteRow;
    }


    private JComponent tweedeRowVariableRecords(JLabel containert) {
        JLabel pa = new JLabel();
        pa.setOpaque(true);
        pa.setBackground(Color.green);
        pa.setLayout(new GridLayout(6, 1));

        String beginDate = "2017-05-9 01:00:00"; // test
        String eindDate = "2018-10-20 23:59:59"; // test
        LocalDate gister = vandaag.minusDays(1);


        //query
        /*
            SELECT * FROM  `pakket`WHERE pakketVerzenddatum <'2018-10-20 23:59:59' AND pakketVerzenddatum >
            '2017-05-9 01:00:00' ORDER BY  `pakket`.`pakketVerzenddatum`DESC LIMIT 0, 30
        */
        try {
            ResultSet totaalAantalPakketten = db.selectSet("SELECT * FROM  `pakket`WHERE pakketVerzenddatum >'" + gister + "' AND pakketVerzenddatum < '" + vandaag + "' limit  6");

            while (totaalAantalPakketten.next()) {
                //maakt strings van query
                String totaalPakkettenPakketID = totaalAantalPakketten.getString("pakketID");
                String pakketVerzenddatum = totaalAantalPakketten.getString("pakketVerzenddatum");
                Timestamp datumVerzend = totaalAantalPakketten.getTimestamp("pakketVerzenddatum");
                String pakketVan = totaalAantalPakketten.getString("pakketBeginpunt");
                String pakketNaar = totaalAantalPakketten.getString("pakketEindpunt");
                //voegt men toe
                tempLabel = new JLabel("Pakket ID " + totaalPakkettenPakketID + " verzenddatum " + pakketVerzenddatum + " van station " + pakketVan + " naar station " + pakketNaar);
                pa.add(tempLabel);


                Timestamp stamp = new Timestamp(System.currentTimeMillis());
                Date huidigeDatum = new Date(stamp.getTime());
                Date recordDatum = new Date(datumVerzend.getTime());

                if (!(huidigeDatum.getDate() - 1 == ((recordDatum.getDate() + 1)))) {
                    System.out.println(recordDatum.getDate() + "<< eerfste datum ||| tweede datum  >> " + huidigeDatum.getDate());
                    System.out.println("foutje");
                }


            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }

        containert.add(pa);
        return containert;
    }


    public ResultSet selectQuery(String selectWhat, String fromWhat, String whereWhat) {
        ResultSet uitslag = db.selectSet("SELECT " + selectWhat + " FROM " + fromWhat + " WHERE " + whereWhat);
        return uitslag;
    }

    public JComponent paginaFooter(JPanel footert) {
        this.footert = footert;
        footert = new JPanel();
        footert.setLayout(new FlowLayout());
        footert.add(new JLabel("OnTrack 2017 - Erik, Gerjan, Mathijs, Nick en Stefan.", JLabel.CENTER));
        return footert;
    }

    @Override
    public void actionPerformed(ActionEvent e) {


        Object o = e.getSource();

        int welkeInhoud = 0;
        if (o == button) { //verschillende recorsds
            JDialog dia = new JDialog();
            welkeInhoud = (comboBox.getSelectedIndex());
            MainPageDetailsRecords ppd = new MainPageDetailsRecords(dia, welkeInhoud);
            ppd.setVisible(true);
        }
        if (o == tekenDialoogStationStatus) { // totaal per station
            index = 0;
            JFrame venster = new JFrame();
            MainPagaDialog dialogFrame = new MainPagaDialog(venster, index);
            dialogFrame.setVisible(true);
        }
        if (o == tekenDialoogDagelijksPakket) { // medewerkers / hoeveel klanten / gemiddelde door loop tijd / leverancier /
            index = 1;
            JFrame venster = new JFrame();
            MainPagaDialog dialogFrame = new MainPagaDialog(venster, index);
            dialogFrame.setVisible(true);
        }
    }

    private void setColors(JLabel x) {
        x.setBorder(BorderFactory.createLineBorder(new Color(121, 218, 24)));
        x.setOpaque(true);
        x.setBackground(new Color(165, 173, 150));

    }


}



