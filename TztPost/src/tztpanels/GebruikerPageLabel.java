/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztpanels;

/**
 * @author nickh
 */

import java.sql.*;
import java.awt.*;
import java.awt.event.*;

import tztdatabase.*;

import javax.swing.*;

public class GebruikerPageLabel extends JLabel implements ActionListener {
    private String nummer, naam;
    private JButton button;
    private Database db;

    public GebruikerPageLabel(String accountID) {
        //Vormgeving label
        this.setPreferredSize(new Dimension(550, 100));
        this.setOpaque(true);
        this.setBackground(new Color(150, 150, 150));
        this.setLayout(new GridLayout(3, 1));
        this.nummer = accountID;

        db = new Database();
        //String id = Integer.toString(accountID);

        ResultSet account = db.selectSet("SELECT * FROM account WHERE accountID=" + accountID);
        try {
            while (account.next()) {
                this.naam = account.getString("accountGebruikersnaam");
            }
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }


        button = new JButton("Details");
        button.addActionListener(this);
        button.setBackground(new Color(50, 255, 50));

        //Toevoegingen
        this.add(new JLabel("Accountnummer: " + nummer));
        this.add(new JLabel("Gebruikersnaam: " + naam));
        this.add(button);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        GebruikerPageDetail gpd = new GebruikerPageDetail(this.naam);
        gpd.setVisible(true);
    }
}
