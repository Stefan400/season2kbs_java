package tztpanels;

import tztdatabase.Database;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by stefa on 5/8/2017.
 */
public class GebruikerPageDetail extends JDialog implements ActionListener {

    private String record;
    private JLabel aanhef, voornaam, tussenvoegsel, achternaam, telnr, email, adres, postcode, woonplaats, notities;
    private JTextField tfAanhef, tfVoornaam, tfTussenvoegsel, tfAchternaam, tfTelnr, tfEmail, tfAdres, tfPostcode, tfWoonplaats;
    private JTextArea tfNotities;
    private JButton editSave, close;
    private int widp, aidp, sollicitant;

    Database db = new Database();

    public GebruikerPageDetail(String record) {
        Font font = new Font("Verdana", Font.BOLD, 12);
        setFont(font);
        this.record = record;
        this.setTitle("Details: " + record);
        this.setModal(true);
        this.setSize(480, 600);
        this.setLayout(new GridLayout(11, 2));
        this.setComponents();
        this.setColors();
    }

    private void setColors() {
        int greyTint = 150;
        aanhef.setBackground(new Color(greyTint, greyTint, greyTint));
        voornaam.setBackground(new Color(greyTint, greyTint, greyTint));
        tussenvoegsel.setBackground(new Color(greyTint, greyTint, greyTint));
        achternaam.setBackground(new Color(greyTint, greyTint, greyTint));
        telnr.setBackground(new Color(greyTint, greyTint, greyTint));
        email.setBackground(new Color(greyTint, greyTint, greyTint));
        adres.setBackground(new Color(greyTint, greyTint, greyTint));
        postcode.setBackground(new Color(greyTint, greyTint, greyTint));
        woonplaats.setBackground(new Color(greyTint, greyTint, greyTint));
        notities.setBackground(new Color(greyTint, greyTint, greyTint));
        editSave.setBackground(new Color(50, 255, 50));
        close.setBackground(new Color(50, 255, 50));
        this.repaint();
    }

    private void setComponents() {
        aanhef = new JLabel("Aanhef:");
        voornaam = new JLabel("Voornaam:");
        tussenvoegsel = new JLabel("tussenvoegsel:");
        achternaam = new JLabel("Achternaam:");
        telnr = new JLabel("Telefoonnummer:");
        email = new JLabel("E-mail:");
        adres = new JLabel("Adres:");
        postcode = new JLabel("Postcode:");
        woonplaats = new JLabel("Woonplaats:");
        notities = new JLabel("Notities:");

        aanhef.setOpaque(true);
        voornaam.setOpaque(true);
        tussenvoegsel.setOpaque(true);
        achternaam.setOpaque(true);
        telnr.setOpaque(true);
        email.setOpaque(true);
        adres.setOpaque(true);
        postcode.setOpaque(true);
        woonplaats.setOpaque(true);
        notities.setOpaque(true);

        tfAanhef = new JTextField();
        tfVoornaam = new JTextField();
        tfTussenvoegsel = new JTextField();
        tfAchternaam = new JTextField();
        tfTelnr = new JTextField();
        tfEmail = new JTextField();
        tfAdres = new JTextField();
        tfPostcode = new JTextField();
        tfWoonplaats = new JTextField();

        tfNotities = new JTextArea();
        JScrollPane scrNotities = new JScrollPane(tfNotities);
        scrNotities.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrNotities.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        editSave = new JButton("Bewerken");
        close = new JButton("Sluiten");

        editSave.addActionListener(this);
        close.addActionListener(this);
        this.fillUp(record);
        tfAanhef.setEditable(false);
        tfVoornaam.setEditable(false);
        tfTussenvoegsel.setEditable(false);
        tfAchternaam.setEditable(false);
        tfTelnr.setEditable(false);
        tfEmail.setEditable(false);
        tfAdres.setEditable(false);
        tfPostcode.setEditable(false);
        tfWoonplaats.setEditable(false);
        tfNotities.setEditable(false);

        this.add(aanhef);
        this.add(tfAanhef);
        this.add(voornaam);
        this.add(tfVoornaam);
        this.add(tussenvoegsel);
        this.add(tfTussenvoegsel);
        this.add(achternaam);
        this.add(tfAchternaam);
        this.add(telnr);
        this.add(tfTelnr);
        this.add(email);
        this.add(tfEmail);
        this.add(adres);
        this.add(tfAdres);
        this.add(postcode);
        this.add(tfPostcode);
        this.add(woonplaats);
        this.add(tfWoonplaats);
        this.add(notities);
        this.add(scrNotities);
        this.add(editSave);
        this.add(close);
    }

    private void fillUp(String username) {
        try {
            int aid = 0;
            int wid = 0;

            ResultSet decideType = db.selectSet("SELECT afzenderID, werknemerID FROM account WHERE accountGebruikersnaam = '" + username + "';");

            while (decideType.next()) {
                aid = decideType.getInt("afzenderID");
                wid = decideType.getInt("werknemerID");
                System.out.println(aid);


            }

            if (aid != 0) {
                ResultSet afzender = db.selectSet("SELECT * FROM afzender WHERE afzenderID =" + aid);
                while (afzender.next()) {
                    this.tfAanhef.setText(afzender.getString("afzenderAanhef"));
                    this.tfVoornaam.setText(afzender.getString("afzenderVoornaam"));
                    this.tfTussenvoegsel.setText(afzender.getString("afzenderTussenvoegsel"));
                    this.tfAchternaam.setText(afzender.getString("afzenderAchternaam"));
                    this.tfTelnr.setText(afzender.getString("afzenderTelefoonnummer"));
                    this.tfEmail.setText(afzender.getString("afzenderEmail"));
                    this.tfAdres.setText(afzender.getString("afzenderAdres"));
                    this.tfWoonplaats.setText(afzender.getString("afzenderWoonplaats"));
                    this.tfPostcode.setText(afzender.getString("afzenderPostcode"));
                }
                aidp = aid;
            } else {
                ResultSet werknemerPre = db.selectSet("SELECT sollicitantID FROM werknemer WHERE werknemerID =" + wid);

                while (werknemerPre.next()) {
                    sollicitant = werknemerPre.getInt("sollicitantID");
                }

                ResultSet werknemer = db.selectSet("SELECT * FROM sollicitant WHERE sollicitantID = " + sollicitant);
                while (werknemer.next()) {
                    this.tfAanhef.setText(werknemer.getString("sollicitantAanhef"));
                    this.tfVoornaam.setText(werknemer.getString("sollicitantVoornaam"));
                    this.tfTussenvoegsel.setText(werknemer.getString("sollicitantTussenvoegsel"));
                    this.tfAchternaam.setText(werknemer.getString("sollicitantAchternaam"));
                    this.tfTelnr.setText(werknemer.getString("sollicitantTelefoonnummer"));
                    this.tfEmail.setText(werknemer.getString("sollicitantEmail"));
                    this.tfAdres.setText(werknemer.getString("sollicitantAdres"));
                    this.tfWoonplaats.setText(werknemer.getString("sollicitantWoonplaats"));
                    this.tfPostcode.setText(werknemer.getString("sollicitantPostcode"));
                    this.tfNotities.setText(werknemer.getString("sollicitantNotities"));
                }
                widp = wid;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source == editSave) {
            if (editSave.getText().equals("Bewerken")) {
                tfAanhef.setEditable(true);
                tfVoornaam.setEditable(true);
                tfTussenvoegsel.setEditable(true);
                tfAchternaam.setEditable(true);
                tfTelnr.setEditable(true);
                tfEmail.setEditable(true);
                tfAdres.setEditable(true);
                tfPostcode.setEditable(true);
                tfWoonplaats.setEditable(true);
                tfNotities.setEditable(true);
                editSave.setText("Opslaan");
                this.repaint();
            } else {
                editSave.setText("Bewerken");
                tfAanhef.setEditable(false);
                tfVoornaam.setEditable(false);
                tfTussenvoegsel.setEditable(false);
                tfAchternaam.setEditable(false);
                tfTelnr.setEditable(false);
                tfEmail.setEditable(false);
                tfAdres.setEditable(false);
                tfPostcode.setEditable(false);
                tfWoonplaats.setEditable(false);
                tfNotities.setEditable(false);

                String table = "";
                String addition = "";
                int number = 0;

                if (aidp != 0) {
                    table = "afzender";
                    number = aidp;
                } else {
                    table = "sollicitant";
                    addition = ", sollicitantNotities = '" + this.tfNotities.getText() + "'";
                    number = sollicitant;
                }

                String query = "UPDATE " + table + " SET "
                        + table + "Aanhef ='" + this.tfAanhef.getText() + "', "
                        + table + "Voornaam ='" + this.tfVoornaam.getText() + "', "
                        + table + "Tussenvoegsel ='" + this.tfTussenvoegsel.getText() + "', "
                        + table + "Achternaam ='" + this.tfAchternaam.getText() + "', "
                        + table + "Telefoonnummer ='" + this.tfTelnr.getText() + "', "
                        + table + "Email ='" + this.tfEmail.getText() + "', "
                        + table + "Adres ='" + this.tfAdres.getText() + "', "
                        + table + "Postcode ='" + this.tfPostcode.getText() + "', "
                        + table + "Woonplaats ='" + this.tfWoonplaats.getText() + "'" + addition +
                        " WHERE " + table + "ID ='" + number + "';";

                db.update(query);
            }
        }
        if (source == close) {
            this.setVisible(false);
        }
    }
}
