package tztpanels;

import java.awt.*;
import java.awt.event.*;
import java.awt.Color;
import java.awt.Dimension;
import java.sql.*;

import tztdatabase.*;

import javax.swing.*;
import javax.xml.transform.Result;

import tztincludes.WrapLayout;

//private ResultSet pakketlijst;

public class PakketPage extends JPanel {
    Database db = new Database();

    public PakketPage() {
        //Layoutpakketpage
        this.setLayout(new WrapLayout());
        this.setBackground(new Color(200, 200, 200));

        //query opvragen
        try {
            ResultSet pakketlijst = db.selectSet("SELECT pakketID FROM pakket ORDER BY pakketID DESC");

            //bij geen result van deze query vroert SQLException
            try {
                //Geeft pakket informatie door
                while (pakketlijst.next()) {
                    final int pakketcode = pakketlijst.getInt("pakketID");
                    PakketPageLabel ppl = new PakketPageLabel(pakketcode);
                    add(ppl);
                }
            } catch (SQLException sqle) {
                //SQL problem
                sqle.printStackTrace();
            }

        } catch (NullPointerException nooutput) {
            //no output error..
            System.out.println("error geen output");

        }


    }
}
