/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tztincludes;

/**
 *
 * @author nickh
 */
public class HashMemory {
    private int length, hashLength;
    private String hashReturn = "";
    private String hashCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
    public HashMemory(int length){
        this.length = length;
    }
    
    public String makeHash(){
        while(hashLength < length){
            int randomNum = 0 + (int)(Math.random() * (hashCharacters.length() - 1));
            
            hashReturn = hashReturn + hashCharacters.charAt(randomNum);
            
            hashLength++;
        }
        
        return hashReturn;
    }
}
