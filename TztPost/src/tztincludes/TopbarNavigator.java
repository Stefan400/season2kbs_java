package tztincludes;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

import tztpostmain.JavaHomepage;

public class TopbarNavigator extends JPanel implements ActionListener {
    private JButton uitloggen;

    public TopbarNavigator() {
        this.setPreferredSize(new Dimension(200, 100));
        this.setBackground(new Color(150, 150, 150));

        //uitlog knop
        uitloggen = new JButton("Uitloggen");
        uitloggen.setBackground(new Color(50, 255, 50));
        this.setLayout(new BorderLayout());
        add(uitloggen, BorderLayout.LINE_END);
        uitloggen.addActionListener(this);


    }

    // Action Listener
    @Override
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        Component c = (Component) e.getSource();
        JavaHomepage frame = (JavaHomepage) SwingUtilities.getRoot(c);
        if (src == uitloggen) {
            frame.dispose();
            frame = new JavaHomepage();
            frame.repaint();
        }
    }
}
