package tztincludes;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;

import tztpanels.LoginDialog;
import tztpostmain.*;

public class SidebarNavigator extends JPanel implements ActionListener {
    private JLabel ingelogd;
    private JLabel naam;
    private JButton home;
    private JButton gebruikers;
    private JButton pakketten;
    private JButton financien;
    private JButton hr;
    private JavaHomepage topframe;

    public SidebarNavigator() {
        this.setPreferredSize(new Dimension(200, 800));
        this.setBackground(new Color(150, 150, 150));


        //naam
        naam = new JLabel();
        add(naam);

        //inloggegevens
        ingelogd = new JLabel("Ingelogd als: ");
        ingelogd.setPreferredSize(new Dimension(190, 100));
        ingelogd.setBackground(new Color(50, 255, 50));
        add(ingelogd);

        //home button
        home = new JButton("HOME");
        home.setPreferredSize(new Dimension(190, 100));
        home.setBackground(new Color(50, 255, 50));
        //home.setBorder(new LineBorder(Color.BLACK));
        home.addActionListener(this);
        add(home);

        //Gebruikers button
        gebruikers = new JButton("GEBRUIKERS");
        gebruikers.setPreferredSize(new Dimension(190, 100));
        gebruikers.setBackground(new Color(50, 255, 50));
        //gebruikers.setBorder(new LineBorder(Color.BLACK));
        gebruikers.addActionListener(this);
        add(gebruikers);

        //Pakketten button
        pakketten = new JButton("PAKKETTEN");
        pakketten.setPreferredSize(new Dimension(190, 100));
        pakketten.setBackground(new Color(50, 255, 50));
        //pakketten.setBorder(new LineBorder(Color.BLACK));
        pakketten.addActionListener(this);
        add(pakketten);

        //financi�n button
        financien = new JButton("FINANCIAN");
        financien.setPreferredSize(new Dimension(190, 100));
        financien.setBackground(new Color(50, 255, 50));
        //financien.setBorder(new LineBorder(Color.BLACK));
        financien.addActionListener(this);
        add(financien);

        //financi�n button
        hr = new JButton("HR-AFDELING");
        hr.setPreferredSize(new Dimension(190, 100));
        hr.setBackground(new Color(50, 255, 50));
        //financien.setBorder(new LineBorder(Color.BLACK));
        hr.addActionListener(this);
        add(hr);


    }

    // Acties na klikken knoppen
    public void actionPerformed(ActionEvent e) {
        Object src = e.getSource();
        Component c = (Component) e.getSource();
        topframe = (JavaHomepage) SwingUtilities.getRoot(c);

        if (src == home) {
            this.topframe.setContentPane(new HomepagePanel(0));
            this.topframe.revalidate();
            this.topframe.repaint();
        }
        if (src == gebruikers) {
            this.topframe.setContentPane(new HomepagePanel(1));
            this.topframe.revalidate();
            this.topframe.repaint();
        }
        if (src == pakketten) {
            this.topframe.setContentPane(new HomepagePanel(2));
            this.topframe.revalidate();
            this.topframe.repaint();
        }
        if (src == financien) {
            this.topframe.setContentPane(new HomepagePanel(3));
            this.topframe.revalidate();
            this.topframe.repaint();
        }
        if (src == hr) {
            this.topframe.setContentPane(new HomepagePanel(4));
            this.topframe.revalidate();
            this.topframe.repaint();
        }
    }

}
