package tztpostmain;

import javax.swing.*;

import tztincludes.*;
import tztpanels.*;

import java.awt.*;

public class HomepagePanel extends BaseLayout {
    public static int pageNo = 0;
        /*
            0 = main
            1 = user
            2 = pakket
            3 = financien
            4 = hr
        */

    private MainPage mainpage = new MainPage();
    private GebruikerPage gebruikerpage = new GebruikerPage();
    private PakketPage pakketpage = new PakketPage();
    private FinancienPage finpage = new FinancienPage();
    private HrAfdelingPage HrPage = new HrAfdelingPage();

    public HomepagePanel(int page) {
        this.pageNo = page;
        this.setPreferredSize(new Dimension(582, 800));
        this.setMaximumSize(new Dimension(582, 800));
        this.setBackground(new Color(255, 255, 0));


        if (pageNo == 0) {
            super.addComponent(mainpage);
        } else if (pageNo == 1) {
            JScrollPane scrPane = new JScrollPane(gebruikerpage, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            super.addComponent(scrPane);
        } else if (pageNo == 2) {
            JScrollPane scrPane = new JScrollPane(pakketpage, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            super.addComponent(scrPane);
        } else if (pageNo == 3) {
            super.addComponent(finpage);
        } else if (pageNo == 4) {
            super.addComponent(HrPage);
        }
    }
}

