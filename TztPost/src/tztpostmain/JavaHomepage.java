package tztpostmain;

import javax.swing.*;

import tztpanels.LoginDialog;

import java.awt.*;

public class JavaHomepage extends JFrame {
    public int pageNo = 0;
    private LoginDialog login = new LoginDialog();
    private Font font ;

    public JavaHomepage() {
        setTitle("Homepage");
        setFont(font);
        boolean isChecked = login.getChecked();
        System.out.println(isChecked);
        if (isChecked) {
            login = null;
        } else {
            login.setVisible(true);
        }
        setSize(800, 800);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(new HomepagePanel(pageNo));
        setVisible(true);
    }

    public static void main(String[] args) {
        JavaHomepage J1 = new JavaHomepage();
    }

    public void setPageNo(int i) {
        this.pageNo = i;
    }
}
