package tztpostmain;

import java.awt.*;
import javax.swing.*;

import tztincludes.SidebarNavigator;
import tztincludes.TopbarNavigator;

public abstract class BaseLayout extends JPanel {
    private final SidebarNavigator sidebar = new SidebarNavigator();
    private final TopbarNavigator topbar = new TopbarNavigator();

    JPanel CenterPanel = new JPanel();


    public BaseLayout() {
        setLayout(new BorderLayout());

        CenterPanel.setLayout(new BorderLayout());
        CenterPanel.add(topbar, BorderLayout.NORTH);

        //sidebar navigator
        add(sidebar, BorderLayout.LINE_START);
        add(CenterPanel, BorderLayout.CENTER);


    }

    public void addComponent(Component comp) {
        this.CenterPanel.add(comp);
    }
}
