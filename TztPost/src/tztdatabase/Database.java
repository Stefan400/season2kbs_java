package tztdatabase;

import java.sql.*;
import java.util.ArrayList;

public class Database {
    //Databaseconnectie vaststellen
    private String _host = "localhost:3307";
    private String _dbName = "ontrack";
    private String _user = "root";
    private String _pass = "usbw";
    //=============================
    private Connection _conn;
    private ArrayList<Object> returnList;

    public Database(){
        String connection = "jdbc:mysql://" + this._host + "/" + this._dbName;
        
        try{
                Class.forName("com.mysql.jdbc.Driver");
                //Database db = new Database("jdbc:mysql://localhost:3307/cupcakes","root","usbw");
                this._conn = DriverManager.getConnection(connection, this._user, this._pass);
                this.returnList = new ArrayList<>();
        }
        catch(Exception e){
                System.out.println();
        }
    }

    public ResultSet selectSet(String statement){
        try {
                Statement stmt = _conn.createStatement();
                ResultSet rs = stmt.executeQuery(statement);

                return rs;
        } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
        }
    }

    public String selectSingle(String statement, String colName){
        try {
                Statement stmt = _conn.createStatement();
                ResultSet rs = stmt.executeQuery(statement);
                String returnable = "";
                while(rs.next()){
                        returnable = rs.getString(colName);
                }
                return returnable;
        } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
        }
    }

    public boolean insert(String statement){
        try {
                Statement stmt = _conn.createStatement();
                boolean returnable = stmt.execute(statement);
                System.out.println("Inserted values in Database: -- " + statement);

                return returnable;
        } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
        }
    }

    public boolean update(String statement){
        try {
                Statement stmt = _conn.createStatement();
                boolean returnable = stmt.execute(statement);
                System.out.println("Updated values in Database: -- " + statement);

                return returnable;
        }
        catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
        }
    }

    public boolean delete(String statement){
        try {
                Statement stmt = _conn.createStatement();
                boolean returnable = stmt.execute(statement);
                System.out.println("Deleted values in Database: -- " + statement);

                return returnable;
        }
        catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return false;
        }
    }


}
//system.exit(0)